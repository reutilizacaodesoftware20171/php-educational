<?php 
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Headers: Content-Type"); ?> <?php

require_once('settings.php');

$query = $_GET["query"];
$tweets = getTweetsWith($query);

$educational = array();
foreach ($tweets as &$tweet) {
	$result = $DatumboxAPI->EducationalDetection($tweet);
	if (!isset($educational[$result])) {
	    $educational[$result] = 0;
	}
	$educational[$result]++;
}

unset($DatumboxAPI);

print json_encode($educational);